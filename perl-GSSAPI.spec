Name:           perl-GSSAPI
Version:        0.28
Release:        27
Summary:        Providing access to the GSSAPIv2 library
License:        GPL-1.0-or-later OR Artistic-1.0-Perl
URL:            https://metacpan.org/release/GSSAPI
Source0:        https://cpan.metacpan.org/authors/id/A/AG/AGROLMS/GSSAPI-%{version}.tar.gz
BuildRequires:  findutils gcc krb5-devel which perl-devel perl-generators
BuildRequires:  perl-interpreter perl(ExtUtils::MakeMaker) >= 6.76 perl(Getopt::Long)
BuildRequires:  perl(constant) perl(Carp) perl(Exporter) perl(ExtUtils::testlib)
BuildRequires:  perl(Test::More) perl(Test::Pod) >= 1.00 perl(XSLoader)

%description
This module can access the routines of the GSSAPI library.

%package_help

%prep
%autosetup -n GSSAPI-%{version} -p1
chmod -c a-x examples/*.pl

%build
perl Makefile.PL INSTALLDIRS=vendor OPTIMIZE="%{optflags}" NO_PACKLIST=1
%make_build

%install
make pure_install DESTDIR=%{buildroot}
%{_fixperms} %{buildroot}/*

%check
make test

%files
%doc Changes README examples/
%{perl_vendorarch}/auto/*
%{perl_vendorarch}/GSSAPI*

%files help
%{_mandir}/man3/*

%changelog
* Sat Jan 18 2025 Funda Wang <fundawang@yeah.net> - 0.28-27
- drop useless perl(:MODULE_COMPAT) requirement

* Thu Dec 12 2019 fengbing <fengbing7@huawei.com> - 0.28-26
- Package init
